angular.module('starter.services', [])

  .service('ApiInteractorServices', function () {

    var base = "http://thecoin.local:3000/";
    var endpoints = {
      login: {
        signIn: "users/sign_in",
        signOut: "users/sign_out"
      },

      human: {
        getCoins: "mobile/get_coins",
        updateLocate: "mobile/update_location",
        updateShake: "mobile/update_shake",
        requestBarter: "mobile/request_barter",
        replyBarter: "mobile/reply_barter",
        attractCoin: "mobile/attract_coin",
        getMessages: "mobile/get_messages",
      },

      coin: {
        getCoinDetail: "mobile/get_coin_detail"
      },

      game: {
        findNearbyCoins: "mobile/find_nearby_coins",
        findNearbyHumans: "mobile/find_nearby_humans"
      }
    };
    var authorizationTokens = {
      userEmail: "",
      userToken: ""
    };


    //TODO: Definir o que acontece quando há erro
    function procSuccess(data){
      if(data.status){
        delete data.status;
        return data;
      }else{
        return procError(data);
      }
    }

    function procError(data){

    }


    function addAuthHeaders( jqXHR ,  settings ){
      if(authorizationTokens.userToken !== "") {
        jqXHR.setRequestHeader("X-User-Email", authorizationTokens.userEmail);
        jqXHR.setRequestHeader("X-User-Token", authorizationTokens.userToken);
      }
    }
    // ve isto - https://docs.angularjs.org/api/ng/service/$http
    function ajaxCall(url, method, data, onSuccess, onError, headers) {
      //Ler isto https://docs.angularjs.org/api/ng/service/$http
      if(typeof(headers) == "undefined"){
        headers = {};
      }

      $.ajax({
        method: method,
        url: url,
        data: data,

        beforeSend: addAuthHeaders,
        headers: headers,

        success: onSuccess,
        dataType: "json"
      }).fail(onError);
    }


    function retrieve(endpointName, data, onSuccess, onError) {
      var url = base + endpointName;
      ajaxCall(url, "GET", data, onSuccess, onError);

    }

    function update(endpointName, data, onSuccess, onError) {
      var url = base + endpointName;
      ajaxCall(url, "POST", data, onSuccess, onError);

    }

    function destroy(endpointName, data, onSuccess, onError) {
      var url = base + endpointName;
      ajaxCall(url, "DELETE", data, onSuccess, onError);
    }



    //ROUTES
    return {
      login: {
        signIn: function (email, password){

          return retrieve(endpoints.login.signIn, {email: email, password: password},
            function(data){
              if(data.login_status == 1){
                authorizationTokens = {
                  userEmail: data.email,
                  userToken: data.authentication_token
                };
                //id?
              }
            },
            procError,
            {"Content-Type": "application/x-www-form-urlencoded"});
        },
        signOut: function (){
          return destroy(endpoints.login.signOut, {}, function(){}  , function(){});
        }
      },

      human: {
        getCoins: function (data, onSuccess, onError) { return retrieve(endpoints.human.getCoins, data, onSuccess, onError); },
        updateLocate: function (data, onSuccess, onError) { return update(endpoints.human.updateLocate, data, onSuccess, onError); },
        updateShake: function (data, onSuccess, onError) { return update(endpoints.human.updateShake, data, onSuccess, onError); },
        requestBarter: function (data, onSuccess, onError) { return update(endpoints.human.requestBarter, data, onSuccess, onError); },
        replyBarter: function (data, onSuccess, onError) { return update(endpoints.human.replyBarter, data, onSuccess, onError); },
        attractCoin: function (data, onSuccess, onError) { return update(endpoints.human.attractCoin, data, onSuccess, onError); },
        getMessages: function (data, onSuccess, onError) { return retrieve(endpoints.human.getMessages, data, onSuccess, onError); }
      },
      coin: {
        getCoinDetail: function (data, onSuccess, onError) { return retrieve(endpoints.coin.getCoinDetail, data, onSuccess, onError); }
      },
      game: {
        findNearbyCoins: function (data, onSuccess, onError) { return retrieve(endpoints.game.findNearbyCoins, data, onSuccess, onError); },
        findNearbyHumans: function (data, onSuccess, onError) { return retrieve(endpoints.game.findNearbyHumans, data, onSuccess, onError); },
      }
    };
  });