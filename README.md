# README #

### Installation - Dependencies

You need Node JS [node js](https://nodejs.org/)

You need gulp installed globally:

```sh
$ npm install -g gulp
```
and Bower installed. Bower is a command line utility.
```sh
$ npm install -g bower
```


### Install Ionic  ###

```sh
$ npm install -g cordova ionic
```

### Run it

**ios**

build
```sh
$ ionic build ios 
```
emulate
```sh
$ ionic emulate ios
```

**ANDROID**

build
```sh
$ ionic build android 
```
emulate
```sh
$ ionic emulate android
```


**locahost**
(bad performance)

```sh
$ ionic serve
```
## help ##

```sh
$ ionic
```



# ngCordova #

```sh
$ bower install ngCordova
```

**Install Plugin**

[see it here](http://ngcordova.com/docs/plugins/)